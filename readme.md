# hexo-deployer-ipfs

This hexo plugin can deploy your files directly to IPFS, using a local node

## Installation
Using npm
```sh
cd 'your hexo project' <br>
npm i --save hexo-deployer-ipfs
```

## Configuration
in your _config.yml file, configure a deployer as below.

 ```yaml
deploy: <br>
  - type: "ipfs" # required
    path: "public" # optional, the folder to be added
    host: "localhost" # optional, the ipfs node
    port: 5001 # optional, the node api port
    protocol: "http" # optional, node api protocol
    key: "self" # optional, used for ipfs.add
    pin: true # optional, if added files should be pinned
```

### Pinning

If pinning is enabled, the plugin will create an `ipfs-cache.json` file in your root directory. You can add this to `.gitignore`. It contains the info required to unpin formerly added but no longer needed files when re-deploying.

### Publishing

The last action will be to publish the base directory on ipfs. This can take 10-60sec on average. Do not stop the process before it has completed.
