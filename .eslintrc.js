module.exports = {
	root: true,
	env: {
		'es6': true,
		'commonjs': true,
		'node': true
	},
	extends: [
		'eslint:recommended'
	],
	parserOptions: {
		ecmaVersion: 2018
	}
};
