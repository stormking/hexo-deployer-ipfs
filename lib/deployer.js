const ipfsAPI = require('ipfs-http-client');
const fs = require('hexo-fs');
const path = require('path');

const IPFS_CACHE_FILE = 'ipfs-cache.json';

module.exports = async(args) => {
	if (args.type !== 'ipfs') return;
	const host = args.host || 'localhost';
	const protocol = args.protocol || 'http';
	const port = args.port || 4001;
	const basePath = args.path || 'public';
	const pin = args.pin || false;
	const key = args.key || 'self';
	let baseStat = await fs.stat(basePath);
	if (!baseStat.isDirectory()) {
		throw new Error(`${basePath} is not a directory`);
	}
	const ipfs = ipfsAPI({ protocol, host, port });
	const files = await listDir(basePath, []);
	console.log(`adding ${files.length} files to ipfs`);
	let res = ipfs.addAll(files, {
		key,
		recursive: true
	});
	let { base, all } = await findBasePathEntry(res, basePath);
	if (pin) {
		await setPins(ipfs, all);
	}
	let { name } = await ipfs.name.publish(base.cid);
	console.log('published to',
		`https://dweb.link/ipns/${name}`,
		`https://gateway.ipfs.io/ipns/${name}`
	);
};

async function setPins(ipfs, entries) {
	let cache = await loadCache();
	let allNew = new Set(entries.map(e => e.cid.toString()));
	let allOld = new Set();
	if (cache.pinned.length) {
		let removed = 0;
		allOld = new Set(cache.pinned);
		for (let cid of allOld) {
			if (!allNew.has(cid)) {
				await ipfs.pin.rm(cid);
				removed += 1;
			}
		}
		console.log(`removed ${removed} of ${allOld.size} old pins`);
	}
	let added = 0;
	for (let cid of allNew) {
		if (!allOld.has(cid)) {
			await ipfs.pin.add(cid, {
				recursive: true
			});
			added += 1;
		}
	}
	console.log(`added ${added} of ${allNew.size} new pins`);
	cache.pinned = Array.from(allNew);
	await saveCache(cache);
}

async function loadCache() {
	let data;
	try {
		let content = await fs.readFile(IPFS_CACHE_FILE, { encoding: 'utf8' });
		data = JSON.parse(content);
	} catch (e) {
		data = {
			pinned: []
		};
	}
	return data;
}
async function saveCache(data) {
	await fs.writeFile(IPFS_CACHE_FILE, JSON.stringify(data), { encoding: 'utf8' });
}

async function findBasePathEntry(fileRes, basePath) {
	let all = [], base;
	for await (let row of fileRes) {
		if (row.path === basePath) {
			base = row;
		}
		all.push(row);
	}
	if (!base) throw new Error(`no matching entry found for ${basePath}`);
	return { base, all };
}

async function listDir(dir, filelist) {
	let files = await fs.readdir(dir);
	for (let file of files) {
		let full = path.join(dir, file);
		let stats = await fs.stat(full);
		if (stats.isDirectory()) {
			filelist = await listDir(full, filelist);
		} else {
			const content = {
				path: full,
				content: fs.createReadStream(full),
				mtime: stats.mtime
			};
			filelist.push(content);
		}
	}
	return filelist;
}
